
Open Sound Control (OSC)
========================

This library allows you to use the OSC content format in your D projects.


TODO
----

 - Adds examples ;
 - Adds more in/out/invariant contracts and more unittests ;
 - A better README file ;
 - More documentation ;


License
-------

This library is under MIT license. You can do what ever you want with this
library (open source to proprietary projects).