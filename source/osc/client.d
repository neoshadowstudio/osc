module osc.client;

import std.socket;

import osc.packet;



/// An OSC client object. It uses a default send address, but it can also
/// send any OSC packet to any given address.
///
class OSCClient
{
	private {
		Socket 	_socket;
		Address _address;
	}



	/// Constructs a new OSC client using a socket and an address.
	///
	this( Socket sock, Address addr )
	{
		_socket = sock;
		_address = addr;
	}

	/// Constructs a new OSC client using an address and creating an UDP socket
	/// for IPv4 addresses.
	///
	this( Address addr )
	{
		this( new Socket( AddressFamily.INET, SocketType.DGRAM ), addr );
	}

	/// Constructs a new OSC client using an IPv4 address and creating an
	/// UDP socket for IPv4 addresses.
	///
	this( string ip, ushort port )
	{
		this( new InternetAddress( ip, port ) );
	}

	/// Constructs a new OSC client using a localhost address with the given
	/// port, and creating an UDP socket for IPv4 addresses.
	///
	this( ushort port )
	{
		this( "localhost", port );
	}



	/// Sends the given packet to the default address.
	///
	OSCClient send( OSCPacket p )
	{
		return sendTo( _address, p );
	}

	/// Sends the given packet to the given address.
	///
	OSCClient sendTo( Address addr, OSCPacket p )
	{
		_socket.sendTo( p.data, addr );
		return this;
	}

}


unittest
{
	import osc.server:OSCServer;
	import osc.message:OSCMessage;
	import core.thread:Thread;
	import std.datetime:dur;

	OSCServer server = new OSCServer( );
	server.bind( 15_050 );

	assert( server.empty );


	OSCClient client = new OSCClient( 15_050 );
	Thread.sleep( dur!"seconds"( 1 ) );


	OSCMessage mesg = new OSCMessage( "/test" );
	mesg.add!float( 440 );

	client.send( mesg );

	while( server.empty )
		Thread.sleep( dur!"seconds"( 1 ) );
	

	OSCMessage received = cast(OSCMessage) server.pop;

	assert( received !is null );

	assert( mesg.address == received.address );
	assert( mesg.types == received.types );
	assert( mesg.at!float( 0 ) == received.at!float( 0 ) );
	
	server.close;
}