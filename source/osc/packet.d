module osc.packet;

import std.range;
import std.conv;
import std.algorithm;
import std.bitmanip;
import std.datetime;
import std.variant;

import osc.address;
import osc.timetag;
import osc.typetag;
import osc.oscstring;
import osc.message;
import osc.bundle;



/// An OSC packet is the base class for the OSC message and bundle.
///
class OSCPacket
{

	/// Parses the given binaray data and returns an OSCPacket (a message or
	/// a bundle).
	///
	static OSCPacket parse( ubyte[] data )
	{
		if( data[0] == '#' )
			return _parseBundle( data );
		else
			return _parseMessage( data );
	}


	/// Converts the packet into a byte array.
	///
	@property abstract ubyte[] data( ) const;




	static protected OSCBundle _parseBundle( ubyte[] data )
	{
		assert( data[0 .. 8] == cast(ubyte[])"#bundle\0" );

		size_t idx = 8;
		OSCBundle res = new OSCBundle( _get!SysTime( data, idx ) );
		idx += 8;

		while( idx < data.length )
		{
			size_t size = _get!int( data, idx );
			idx += 4;

			ubyte[] msgdata = data[ idx .. idx + size ];
			OSCMessage msg = _parseMessage( msgdata );

			res.messages ~= msg;
			idx += size;

			idx += 4 - idx % 4;
		}

		return res;
	}

	static protected OSCMessage _parseMessage( ubyte[] data )
	{
		size_t idx;

		OSCMessage res = new OSCMessage( _getAddress( data, 0 ) );
		idx += data.countUntil(',');		

		assert( idx % 4 == 0, "Misaligned OSC packet." );

		string types = _getTypes( data, idx );
		idx += 1 + types.length;



		foreach( t; types )
		{
			switch( t )
			{
			case '\0':
				break;
			
			case TypeTag.Integer:
				res.add!int( _get!int( data, idx ) );
				idx += 4;
				
				break;
			
			case TypeTag.Float:
				res.add!float( _get!float( data, idx ) );
				idx += 4;
				break;
			
			case TypeTag.String:
				string v = _get!string( data, idx );
				idx += v.length;
				res.add!string( v.stripRight( '\0' ) );
				break;
			
			case TypeTag.Blob:
				ubyte[] blob = _get!(ubyte[])( data, idx );
				res.add!(ubyte[])( blob );

				idx += 4 + blob.length + (4 - blob.length % 4);
				break;
			
			case TypeTag.TimeTag:
				res.add!SysTime( _get!SysTime( data, idx ) );
				idx += 8;
				break;
			
			case TypeTag.True:
				res.add!bool( true );
				break;
			
			case TypeTag.False:
				res.add!bool( false );
				break;
			
			case TypeTag.Null:
				res.addNull;
				break;
			
			case TypeTag.Impulse:
				res.addImpulse;
				break;
			
			default:
				assert( 0 );
			}

			while( idx % 4 != 0 )
				idx ++;
		}

		return res;
	}



	static protected Address _getAddress( ubyte[] data, size_t idx ) 
	{
		size_t i = idx;

		for( ; i < data.length; i += 4 )
		{
			if( data[i] == ',' )
			{
				if( i == 0 )
					return Address( "" );
				
				return Address( data[ idx .. i ].dup
					.stripRight( 0 )
					.map!( c => cast(char) c )
					.array );
			}
		}

		throw new Exception( "Invalid packet : no comma found." );
	}

	static protected TypeTagString _getTypes( ubyte[] data, size_t idx ) 
	{
		size_t i = idx + 4;

		for( ; i < data.length; i += 4 )
		{
			if( data[ i - 1 ] == 0 )
			{
				return cast(TypeTagString) data[ idx+1 .. i ].dup;
			}
		}

		throw new Exception( "Invalid packet : type tag string not terminated." );
	}

	static protected T _get( T )( ubyte[] data, size_t idx ) 
	{
		return data.peek!T( idx );
	}

	static protected T _get( T:string )( ubyte[] data, size_t idx ) 
	{
		size_t i = idx + 4;

		for( ; i - 1 < data.length; i += 4 )
		{
			if( data[ i - 1 ] == 0 )
			{
				return cast(string) data[ idx .. i ].dup;
			}
		}	

		throw new Exception( "Invalid packet : string not terminated." );
	}

	static protected T _get( T:ubyte[] )( ubyte[] data, size_t idx ) 
	{
		size_t size = data.peek!int( idx );
		return data[ idx + 4 .. idx + size + 4 ].dup;
	}

	static protected T _get( T:SysTime )( ubyte[] data, size_t idx ) 
	{
		return data[ idx .. idx + 8 ].toSysTime;
	}


	static protected ubyte[] _set( T )( const(T) v ) 
	{
		ubyte[] buf = [0, 0, 0, 0];
		buf.write!T( v, 0 );
		return buf;
	}

	static protected ubyte[] _set( T:string )( const(T) v ) 
	{
		return v.toOSC;
	}

	static protected ubyte[] _set( T:ubyte[] )( const(T) v ) 
	{
		ubyte[] buf = _set!int( cast(int) v.length );
		buf ~= v;

		return buf ~ (0
			.repeat( 4 - buf.length % 4 )
			.map!( x => x.to!ubyte )
			.array);
	}

	static protected ubyte[] _set( T:SysTime )( const(T) v ) 
	{
		return v.toOSC;
	}

}

unittest
{
	ubyte[] raw = [
		0x2f, 0x6f, 0x73, 0x63,
		0x69, 0x6c, 0x6c, 0x61,
		0x74, 0x6f, 0x72, 0x2f,
		0x34, 0x2f, 0x66, 0x72,
		0x65, 0x71, 0x75, 0x65,
		0x6e, 0x63, 0x79, 0x00,
		0x2c, 0x66, 0x00, 0x00,
		0x43, 0xdc, 0x00, 0x00
	];

	OSCMessage msg = cast(OSCMessage) OSCPacket.parse( raw );

	assert( msg.address.toString == "/oscillator/4/frequency" );
	assert( msg.length == 1 );
	assert( msg.isFloat( 0 ) );
	assert( msg.at!float( 0 ) == 440 );
}

unittest
{
	ubyte[] raw = [
		0x23, 0x62, 0x75, 0x6e,
		0x64, 0x6c, 0x65, 0x00,
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x01,
		0x00, 0x00, 0x00, 0x20,
		0x2f, 0x6f, 0x73, 0x63,
		0x69, 0x6c, 0x6c, 0x61,
		0x74, 0x6f, 0x72, 0x2f,
		0x34, 0x2f, 0x66, 0x72,
		0x65, 0x71, 0x75, 0x65,
		0x6e, 0x63, 0x79, 0x00,
		0x2c, 0x66, 0x00, 0x00,
		0x43, 0xdc, 0x00, 0x00
	];

	OSCBundle bdl = cast(OSCBundle) OSCPacket.parse( raw );

	assert( bdl.timetag == immediateSysTime );
	assert( bdl.messages.length == 1 );

	OSCMessage msg = bdl.messages[ 0 ];

	assert( msg.address.toString == "/oscillator/4/frequency" );
	assert( msg.types == "f" );
	assert( msg.at!float( 0 ) == 440 );
}