module osc.oscstring;

import std.range;
import std.conv;
import std.algorithm;


private T addNullSuffix( T:string )( T str )
{
	return str ~ ('\0'
		.repeat( 4 - str.length % 4 )
		.map!( c => cast(immutable(char)) c )
		.array);
}

private T addNullSuffix( T:ubyte[] )( T str )
{
	return cast(T) addNullSuffix( cast(string) str );
}



/// Converts a string to an OSC string.
///
ubyte[] toOSC( string str )
{
	return str
		.addNullSuffix
		.map!( c => c.to!char.to!ubyte )
		.array;
}

/// Converts an OSC string to a string.
///
string fromOSC( ubyte[] str )
{
	return str
		.stripRight( 0 )
		.map!( c => c.to!char)
		.to!string;
}



unittest
{
	assert("osc".addNullSuffix == "osc\0");
	assert("data".addNullSuffix == "data\0\0\0\0");
	assert(",iisff".addNullSuffix == ",iisff\0\0");
	assert(",i".addNullSuffix == ",i\0\0");
}

unittest
{
	assert( "osc".toOSC == 
		['o', 's', 'c', 0] );
	assert( "data".toOSC == 
		['d', 'a', 't', 'a', 0, 0, 0, 0] );
	assert( ",iisff".toOSC ==
		[',', 'i', 'i', 's', 'f', 'f', 0, 0] );
	assert( ",i".toOSC ==
		[',', 'i', 0, 0] );
}