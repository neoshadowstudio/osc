module osc.typetag;

import std.algorithm;
import std.array;
import std.conv;
import std.datetime;


alias TypeTagString = string;


/// Type tag for each supported tags.
///
enum TypeTag : char
{
	Integer = 'i',
	Float = 'f',
	String = 's',
	Blob = 'b',
	True = 'T',
	False = 'F',
	Null = 'N',
	Impulse = 'I',
	TimeTag = 't',
}


/// Constructs a TypeTag string using a list of types.
///
TypeTagString build( T... )( )
{
	TypeTagString res;

	foreach( t; T )
	{
		static if( is(t == int) )
			res = res.add!int;
		else static if( is(t == float) )
			res = res.add!float;
		else static if( is(t == string) )
			res = res.add!string;
		else static if( is(t == ubyte[]) )
			res = res.add!(ubyte[]);
		else static if( is(typeof(t) == typeof(null)) )
			res = res.addNull;
		else static if( is(typeof(t) == bool) )
			res = res.add!bool( t );
		else static if( is(typeof(t) == char) && t is 'I' )
			res = res.addImpulse;
		else static if( is(t == SysTime) )
			res = res.add!SysTime;
		else
			static assert( 0, "Invalid type." );
	}

	return res;
}


/// Adds an integer type tag.
///
TypeTagString add(T:int)(TypeTagString str)
{
	return str ~ TypeTag.Integer;
}

/// Adds a float type tag.
///
TypeTagString add(T:float)(TypeTagString str)
{
	return str ~ TypeTag.Float;
}

/// Adds a string type tag.
///
TypeTagString add(T:string)(TypeTagString str)
{
	return str ~ TypeTag.String;
}

/// Adds a blob type tag.
///
TypeTagString add(T:ubyte[])(TypeTagString str)
{
	return str ~ TypeTag.Blob;
}

/// Adds a bool type tag.
///
TypeTagString add(T:bool)(TypeTagString str, T v)
{
	if( v )
		return str ~ TypeTag.True;
	else
		return str ~ TypeTag.False;
}

/// Adds a null type tag.
///
TypeTagString addNull(TypeTagString str)
{
	return str ~ TypeTag.Null;
}

/// Adds an impulse type tag.
///
TypeTagString addImpulse(TypeTagString str)
{
	return str ~ TypeTag.Impulse;
}

/// Adds a timetag type tag.
///
TypeTagString add(T:SysTime)(TypeTagString str)
{
	return str ~ TypeTag.TimeTag;
}



unittest
{
	TypeTagString str;

	str = str.add!int
		.add!float
		.add!string
		.add!(ubyte[])
		.add!bool(true)
		.add!bool(false)
		.addNull
		.addImpulse
		.add!SysTime;
	
	assert( str == "ifsbTFNIt" );
}

unittest
{
	TypeTagString str = build!( int, float, string, ubyte[], true, false, null, 
		'I', SysTime)();

	assert( str == "ifsbTFNIt" );
}