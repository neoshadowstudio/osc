module osc.message;

import std.range;
import std.conv;
import std.algorithm;
import std.variant;
import std.datetime;

import osc.packet;
import osc.address;
import osc.typetag;
import osc.oscstring;



/// Supported types for an OSC argument.
///
alias OSCArgument = Algebraic!(
	int, 
	float, 
	string, 
	ubyte[], 
	bool, 
	typeof(null),
	char,
	SysTime
);



/// An OSC message contains an address and a set of arguments.
///
class OSCMessage : OSCPacket
{

	private {
		Address				_address;
		TypeTagString		_types;
		OSCArgument[]		_arguments;
	}



	/// Constructs a new empty OSC message.
	///
	this( )
	{
		_address = Address( [], false );
	}

	/// Constructs a new OSC message using an address.
	///
	this( string addr )
	in
	{
		assert( addr.length > 0 );
	}
	do
	{
		this( );
		_address = Address( addr );
	}
	
	/// Constructs a new OSC message using an address.
	///
	this( Address addr )
	{
		this( );
		_address = Address( addr );
	}

	/// Constructs a new OSC message by cloning another one.
	///
	this( const(OSCMessage) copy )
	{
		this( );
		_address = Address( copy._address );
		_arguments = copy._arguments.dup;
	}


	/// Gets the message's address.
	///
	@property Address address( ) const
	{
		return Address( _address );
	}

	/// Sets the message's address.
	///
	@property void address( Address addr )
	{
		_address = Address( addr );
	}

	/// Gets the argment's types.
	///
	@property const(TypeTagString) types( ) const
	{
		return _types;
	}

	/// Gets the message's argument count.
	///
	@property size_t length( ) const
	{
		return _arguments.length;
	}



	/// Duplicates the current message.
	///
	OSCMessage dup( ) const
	{
		return new OSCMessage( this );
	}



	/// Gets the type of the argument at the given index.
	///
	TypeTag typeAt( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
	}
	do
	{
		return cast(TypeTag) _types[ idx ];
	}

	/// Gets the argument at the given index.
	///
	OSCArgument argumentAt( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
	}
	do
	{
		return _arguments[ idx ];
	}



	/// Checks if the given argument is an integer.
	///
	bool isInt( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
	}
	do
	{
		return _types[ idx ] == TypeTag.Integer;
	}

	/// Checks if the given argument is a float.
	///
	bool isFloat( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
	}
	do
	{
		return _types[ idx ] == TypeTag.Float;
	}

	/// Checks if the given argument is an string.
	///
	bool isString( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
	}
	do
	{
		return _types[ idx ] == TypeTag.String;
	}

	/// Checks if the given argument is an blob.
	///
	bool isInt( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
	}
	do
	{
		return _types[ idx ] == TypeTag.Blob;
	}

	/// Checks if the given argument is true.
	///
	bool isTrue( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
	}
	do
	{
		return _types[ idx ] == TypeTag.True;
	}

	/// Checks if the given argument is false.
	///
	bool isFalse( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
	}
	do
	{
		return _types[ idx ] == TypeTag.False;
	}

	/// Checks if the given argument is a boolean.
	///
	bool isBool( size_t idx ) const
	{
		return isTrue( idx ) || isFalse( idx );
	}

	/// Checks if the given argument is null.
	///
	bool isNull( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
	}
	do
	{
		return _types[ idx ] == TypeTag.Null;
	}

	/// Checks if the given argument is an impulse.
	///
	bool isImpulse( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
	}
	do
	{
		return _types[ idx ] == TypeTag.Impulse;
	}

	/// Checks if the given argument is a timetag.
	///
	bool isTimeTag( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
	}
	do
	{
		return _types[ idx ] == TypeTag.TimeTag;
	}



	/// Gets the argument at the given index.
	///
	T at( T:Variant )( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
	}
	do
	{
		return _arguments[ idx ];
	}

	/// Gets the integer at the given index.
	///
	T at( T:int )( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
		assert( _types[ idx ] == TypeTag.Integer );
	}
	do
	{
		return _arguments[ idx ].get!T;
	}

	/// Gets the float at the given index.
	///
	T at( T:float )( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
		assert( _types[ idx ] == TypeTag.Float );
	}
	do
	{
		return _arguments[ idx ].get!T;
	}

	/// Gets the string at the given index.
	///
	T at( T:string )( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
		assert( _types[ idx ] == TypeTag.String );
	}
	do
	{
		return _arguments[ idx ].get!T;
	}

	/// Gets the blob at the given index.
	///
	T at( T:ubyte[] )( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
		assert( _types[ idx ] == TypeTag.Blob );
	}
	do
	{
		return _arguments[ idx ].get!T.dup;
	}

	/// Gets the bool at the given index.
	///
	T at( T:bool )( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
		assert( _types[ idx ] == TypeTag.True || _types[ idx ] == TypeTag.False );
	}
	do
	{
		return _arguments[ idx ].get!T;
	}

	/// Gets the timetag at the given index.
	///
	T at( T:SysTime )( size_t idx ) const
	in
	{
		assert( idx < _arguments.length );
		assert( _types[ idx ] == TypeTag.TimeTag );
	}
	do
	{
		return _arguments[ idx ].get!T;
	}



	/// Adds an argument to the message.
	///
	OSCMessage add( T )( T v )
	{
		_types = _types.add!T;
		OSCArgument va = v;
		_arguments ~= va;

		return this;
	}

	/// Adds a bool to the message.
	///
	OSCMessage add( T:bool )( T v )
	{
		_types = _types.add!bool( v );
		OSCArgument va = v;
		_arguments ~= va;

		return this;
	}

	/// Adds a null to the message.
	///
	OSCMessage addNull( )
	{
		_types = _types.addNull;
		OSCArgument va = null;
		_arguments ~= va;

		return this;
	}

	/// Adds an impulse to the message.
	///
	OSCMessage addImpulse( )
	{
		_types = _types.addImpulse;
		OSCArgument va = 'I';
		_arguments ~= va;

		return this;
	}



	@property override ubyte[] data( ) const
	{
		ubyte[] res;
		string types = "," ~ _types;

		res ~= _address.toString.toOSC ~ types.toOSC;

		int i;
		foreach( t; _types )
		{
			switch( t )
			{
			case TypeTag.Integer:
				res ~= _set!int( _arguments[ i ].get!int );
				break;
			
			case TypeTag.Float:
				res ~= _set!float( _arguments[ i ].get!float );
				break;
			
			case TypeTag.String:
				res ~= _set!string( _arguments[ i ].get!string );
				break;
			
			case TypeTag.Blob:
				res ~= _set!(ubyte[])( _arguments[ i ].get!(ubyte[]) );
				break;
			
			case TypeTag.TimeTag:
				res ~= _set!SysTime( _arguments[ i ].get!SysTime );
				break;

			case TypeTag.True:
			case TypeTag.False:
			case TypeTag.Null:
			case TypeTag.Impulse:
				break;
			
			default:
				assert( 0 );
			}

			i += 1;
		}

		return res;
	}

}


unittest 
{
	ubyte[] raw = [
		0x2f, 0x6f, 0x73, 0x63,
		0x69, 0x6c, 0x6c, 0x61,
		0x74, 0x6f, 0x72, 0x2f,
		0x34, 0x2f, 0x66, 0x72,
		0x65, 0x71, 0x75, 0x65,
		0x6e, 0x63, 0x79, 0x00,
		0x2c, 0x66, 0x00, 0x00,
		0x43, 0xdc, 0x00, 0x00
	];

	OSCMessage msg = new OSCMessage( "/oscillator/4/frequency" );
	msg.add!float( 440.0 );

	assert( msg.data( ) == raw );

	OSCMessage parsed = cast(OSCMessage) OSCPacket.parse( raw );

	assert( parsed.address.toString == "/oscillator/4/frequency" );
	assert( parsed.length == 1 );
	assert( parsed.isFloat( 0 ) );
	assert( parsed.at!float( 0 ) == 440 );
}

unittest
{
	import std.math : abs;

	OSCMessage initial = new OSCMessage( "/unittest/test" );
	initial.add!int( 42 );
	initial.add!float( 3.14 );
	initial.add!string( "test" );
	initial.add!(ubyte[])( cast(ubyte[]) "ubyte" );
	initial.add!bool( true );
	initial.add!bool( false );
	initial.addNull;
	initial.addImpulse;
	SysTime currTime = Clock.currTime;
	initial.add!SysTime( currTime );

	ubyte[] raw = initial.data( );

	OSCMessage parsed = cast(OSCMessage) OSCPacket.parse( raw );

	assert( parsed.address == initial.address );
	assert( parsed.types == initial.types );
	assert( parsed.at!int( 0 ) == 42 );
	assert( abs( parsed.at!float( 1 ) - 3.14 ) < 0.001 );
	assert( parsed.at!string( 2 ) == "test" );
	assert( parsed.at!(ubyte[])( 3 ) == cast(ubyte[]) "ubyte" );
	assert( parsed.at!bool( 4 ) == true );
	assert( parsed.at!bool( 5 ) == false );
	assert( parsed.isNull( 6 ) );
	assert( parsed.isImpulse( 7 ) );
	assert( parsed.at!SysTime( 8 ) == currTime );
}

unittest
{
	import std.math : abs;

	ubyte[] raw = [
		0x2f, 0x66, 0x6f, 0x6f,
		0x00, 0x00, 0x00, 0x00,
		0x2c, 0x69, 0x69, 0x73,
		0x66, 0x66, 0x00, 0x00,
		0x00, 0x00, 0x03, 0xe8,
		0xff, 0xff, 0xff, 0xff,
		0x68, 0x65, 0x6c, 0x6c,
		0x6f, 0x00, 0x00, 0x00,
		0x3f, 0x9d, 0xf3, 0xb6,
		0x40, 0xb5, 0xb2, 0x2d,
	];

	OSCMessage a = cast(OSCMessage) OSCPacket.parse( raw );

	assert( a.address.toString == "/foo" );
	assert( a.length == 5 );
	assert( a.types == "iisff" );
	assert( a.at!int( 0 ) == 1000 );
	assert( a.at!int( 1 ) == -1 );
	assert( a.at!string( 2 ) == "hello" );
	assert( abs( a.at!float( 3 ) - 1.234 ) < 0.001 );
	assert( abs( a.at!float( 4 ) - 5.678 ) < 0.001 );

	const OSCMessage b = new OSCMessage( "/foo" )
		.add!int( 1000 )
		.add!int( -1 )
		.add!string( "hello" )
		.add!float( 1.234 )
		.add!float( 5.678 );
	
	assert( b.data == raw );
}