module osc.server;

import std.socket;
import core.thread;
import core.sync.mutex;

import osc.message;
import osc.bundle;
import osc.packet;



/// A server receiving OSC packets.
///
class OSCServer
{

	private {
		Socket			_listener;
		OSCMessage[]	_buffer;

		Mutex			_bufferLock;
		Thread			_thread;

		bool			_running;
	}



	/// Constructs a new server using an already created socket.
	///
	this( Socket sock )
	{
		_listener = sock;
		_bufferLock = new Mutex( );

		_listener.setOption( SocketOptionLevel.SOCKET, SocketOption.RCVTIMEO, dur!"seconds"( 1 ) );
	}

	/// Constructs a new server using an UDP socket for IPv4 addresses.
	///
	this( )
	{
		this( new Socket( AddressFamily.INET, SocketType.DGRAM ) );
	}



	/// Binds the server to the given address.
	///
	void bind( Address addr )
	in
	{
		assert( _thread is null );
	}
	do
	{
		_listener.bind( addr );
		_thread = new Thread( () => _receive() ).start;
	}

	/// Binds the server to the given IPv4 IP and port.
	///
	void bind( string ip, ushort port )
	in
	{
		assert( _thread is null );
	}
	do
	{
		_listener.bind( new InternetAddress( ip, port ) );
		_thread = new Thread( () => _receive() ).start;
	}

	/// Binds the server to the "localhost" IP using the given local port.
	///
	void bind( ushort port )
	in
	{
		assert( _thread is null );
	}
	do
	{
		_listener.bind( new InternetAddress( "localhost", port ) );
		_thread = new Thread( () => _receive() ).start;
	}



	/// Closes the server.
	///
	void close( )
	{
		_running = false;

		_listener.close;
		_thread.join;
	}



	/// Gets the next message received.
	///
	OSCMessage pop( )
	{
		if( _buffer.length == 0 )
			return null;

		_bufferLock.lock;
		scope( exit ) 
			_bufferLock.unlock;
		
		OSCMessage msg = _buffer[ 0 ];
		_buffer = _buffer[ 1 .. $ ];

		return msg;
		
	}

	/// Checks if some messages are in the server buffer.
	///
	bool empty( )
	{
		return _buffer.length == 0;
	}




	private void _receive( ) {
		ubyte[16_192] buffer;

		_running = true;

		while( _running ) {
			
			ptrdiff_t size = _listener.receive( buffer );

			if( size <= 0 )
				continue;

			OSCMessage[] received;
			OSCPacket p = OSCPacket.parse( buffer[ 0 .. size ] );

			if( cast(OSCBundle) p !is null )
				received = ( cast(OSCBundle) p ).messages;
			else
				received ~= cast(OSCMessage)p;

			_bufferLock.lock;
			foreach( m; received )
				_buffer ~= m;
			_bufferLock.unlock;
		}
	}

}