module osc.bundle;

import std.range;
import std.conv;
import std.algorithm;
import std.datetime;

import osc.address;
import osc.timetag;
import osc.typetag;
import osc.oscstring;
import osc.message;
import osc.packet;



/// A bundle of multiple OSC messages.
///
class OSCBundle : OSCPacket
{

	private {
		SysTime 		_timetag;
		
	}

	/// List of messages contained in the bundle.
	///
	OSCMessage[]	messages;



	/// Constructs a new OSC bundle.
	///
	this( SysTime timetag )
	{
		_timetag = timetag;
	}



	/// Gets the timetag associated to this bundle.
	///
	@property const(SysTime) timetag( ) const
	{
		return _timetag;
	}

	/// Sets the timetag associated to this bundle.
	///
	@property void timetag( SysTime v )
	{
		_timetag = v;
	}



	@property override ubyte[] data( ) const
	{
		ubyte[] res;

		res ~= "#bundle".toOSC;
		res ~= _set!SysTime( _timetag );

		foreach( msg; messages )
		{
			ubyte[] data = msg.data;

			res ~= _set!int( cast(int)data.length );
			res ~= data;

			if( res.length % 4 != 0 )
			{
				res ~= 0
					.repeat( 4 - res.length % 4 )
					.map!( x => cast(ubyte) x )
					.array;
			}
		}

		return res;
	}

}


unittest
{
	immutable ubyte[] raw = [
		0x23, 0x62, 0x75, 0x6e,
		0x64, 0x6c, 0x65, 0x00,
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x01,
		0x00, 0x00, 0x00, 0x20,
		0x2f, 0x6f, 0x73, 0x63,
		0x69, 0x6c, 0x6c, 0x61,
		0x74, 0x6f, 0x72, 0x2f,
		0x34, 0x2f, 0x66, 0x72,
		0x65, 0x71, 0x75, 0x65,
		0x6e, 0x63, 0x79, 0x00,
		0x2c, 0x66, 0x00, 0x00,
		0x43, 0xdc, 0x00, 0x00
	];

	OSCBundle bundle = new OSCBundle( immediateSysTime );

	OSCMessage msg = new OSCMessage( "/oscillator/4/frequency" );
	msg.add!float( 440.0 );

	bundle.messages ~= msg;

	assert( bundle.data == raw );
}