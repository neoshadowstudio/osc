module osc.timetag;

import std.datetime;
import std.conv;
import std.bitmanip;



/// Immediate time tag.
///
immutable ubyte[] immediateTimeTag = [0, 0, 0, 0, 0, 0, 0, 1];
/// SysTime used for using the immediate time tag.
immutable SysTime immediateSysTime = SysTime( DateTime( 1900, 1, 1 ), UTC( ) );



/// Converts a SysTime to an OSC timetag.
///
ubyte[] toOSC( SysTime st )
{
	if( st == immediateSysTime )
		return immediateTimeTag.dup;

	immutable timestamp = st - SysTime( DateTime( 1900, 1, 1 ), UTC( ) );

	immutable ipart = timestamp.total!"seconds";
	immutable fpart = timestamp.total!"nsecs"%(10^^9);

	ubyte[] buffer = [0, 0, 0, 0, 0, 0, 0, 0];
	buffer.write!uint( ipart.to!uint, 0 );
	buffer.write!uint( fpart.to!uint, 4 );

	return buffer;
}

/// Converts an OSC timetag to a SysTime.
///
SysTime toSysTime( const(ubyte[]) buf )
{
	if( buf == immediateTimeTag )
		return immediateSysTime;

	immutable ipart = buf.peek!uint( 0 );
	immutable fpart = buf.peek!uint( 4 );

	return SysTime( DateTime( 1900, 1, 1 ), UTC( ) ) 
		+ dur!"seconds"(ipart) 
		+ dur!"nsecs"(fpart);
}


/// Checks if the given SysTime is an immediate time tag.
///
bool isImmediate( in const(SysTime) st )
{
	return st == immediateSysTime;
}



unittest
{
	assert( immediateSysTime.isImmediate );

	assert( immediateSysTime.toOSC == immediateTimeTag );
	assert( immediateTimeTag.toSysTime == immediateSysTime );
}