module osc.address;

import std.string;
import std.array;
import std.algorithm;
import std.regex;



/// A parsed OSC address, splitted in multiple parts.
///
struct Address
{

	private {
		bool 		_isContainer;
		string[]	_parts;
	}



	/// Constructs a new address from a string.
	///
	this( string addr )
	in
	{
		assert( addr.length > 0 );
	}
	do
	{
		_parts = addr
			.split( "/" )
			.filter!( p => !p.empty )
			.array;
		
		_isContainer = addr[ $-1 ] == '/';
	}

	/// Constructs a new address from another one.
	///
	this( const(Address) copy )
	{
		_parts = copy._parts.dup;
	}

	/// Constructs a new address from a container list.
	///
	this( const(string)[] parts, bool endsWithMethod = true )
	in
	{
		if( endsWithMethod )
			assert( parts.length > 0 );
	}
	do
	{
		_parts = parts.dup;
		_isContainer = !endsWithMethod;
	}



	/// Checks if the given address match the current one.
	///
	bool match( const(Address) addr ) const
	{
		if( length != addr.length )
			return false;
		
		if( isContainer != addr.isContainer )
			return false;
		
		for( int i; i < length; i++ )
		{
			string r = "^" ~ _parts[ i ] ~ "$";
			r = r.replace( "]*", "]#");
			r = r.replace( "*", "[a-zA-Z0-9%-]*" );
			r = r.replace( "[!", "[^" );
			r = r.replace( "{", "(" );
			r = r.replace( "}", ")" );
			r = r.replace( ",", "|" );
			r = r.replace( "]#", "]*" );


			if( !matchFirst( addr[i], r ) )
				return false;
		}

		return true;
	}



	/// Is the current address "/" ?
	///
	@property bool isRoot( ) const
	{
		return _parts.length == 0;
	}

	/// Is the current address a container ?
	///
	@property bool isContainer( ) const
	{
		return _isContainer;
	}

	/// Is the current address a method ?
	///
	@property bool isMethod( ) const
	{
		return !_isContainer;
	}



	/// Returns the address of the container.
	///
	/// If the current address is a container, then the function return 
	/// a copy of the current address.
	///
	/// If the current address is a method, then the function return the 
	/// container of the method.
	///
	@property Address container( ) const
	{
		if( isContainer )
			return Address( this );
		else
			return Address( _parts[ 0 .. $-1 ], false );
	}

	/// Returns the method's name of the address.
	///
	@property string method( ) const
	in
	{
		assert( isMethod );
	}
	do
	{
		return _parts[ $-1 ];
	}

	/// Sets the method's name of the address.
	///
	/// See also `.setMethod`.
	///
	@property void method( string m )
	{
		setMethod( m );
	}



	/// Converts the address to a string.
	///
	string toString( ) const
	{
		if( isRoot )
			return "/";

		string res = "";

		foreach( part; _parts )
			res ~= "/" ~ part;

		if( isContainer )
			res ~= "/";

		return res;
	}



	//======= Input Range



	/// Checks if the container range is empty.
	///
	/// Note: This function can't return `true` just after the constructor call.
	///
	@property bool empty( ) const
	{
		return _parts.length == 0;
	}

	/// Gets the first container of the address.
	///
	@property string front( ) const
	in
	{
		assert( !empty );
	}
	do
	{
		return _parts[ 0 ];
	}

	/// Removes the first container of the address.
	///
	void popFront( )
	in
	{
		assert( !empty );
	}
	do
	{
		_parts = _parts[ 1 .. $ ];
	}



	//======= Forward Range



	/// Copies the container range.
	///
	Address save( ) const
	{
		return Address( this );
	}



	//======= Bidirectional Range



	/// Gets the last container (or the method) of the address.
	///
	@property string back( ) const
	in
	{
		assert( !empty );
	}
	do
	{
		return _parts[ $-1 ];
	}

	/// Removes the last container (or the method) of the address.
	///
	/// Calling this method on a method address change it to a container 
	/// address (`.isMethod == false` after the call).
	///
	void popBack( )
	in
	{
		assert( !empty );
	}
	do
	{
		_parts = _parts[ 0 .. $-1 ];

		if( isMethod )
			_isContainer = true;
	}



	//======= Random Access Range



	/// Access to a container in the address.
	///
	string opIndex( size_t idx ) const
	in
	{
		assert( idx < _parts.length );
	}
	do
	{
		return _parts[ idx ];
	}

	/// Returns the number of containers (including the method) in the
	/// address.
	@property size_t length( ) const
	{
		return _parts.length;
	}



	//====== Modifying the address



	/// Adds a container to the start of the address.
	///
	Address* pushFront( string cont )
	{
		_parts = [cont] ~ _parts;
		return &this;
	}

	/// Adds a container to the end of the address.
	///
	/// When called on a method address, it becomes a container address.
	///
	Address* pushBack( string cont )
	{
		_parts = _parts ~ [cont];

		if( isMethod )
			_isContainer = true;

		return &this;
	}



	/// Sets the method of the address.
	///
	/// If the address is a container address, then it becomes a method address
	/// and the method is added to the end of the address.
	///
	/// If the address is already a method address, then the method's name
	/// is changed.
	///
	Address* setMethod( string m )
	{
		if( isMethod )
			_parts[ $-1 ] = m;
		else
		{
			_isContainer = false;
			_parts ~= m;
		}

		return &this;
	}


	//====== Operators


	/// Adds a container at the end of the address.
	///
	Address opBinary( const(string) op )( string rhs ) const if( op == "+" )
	{
		Address res = Address( this );
		res.pushBack( rhs );
		return res;
	}

	/// Checks if the object is the same as the current instance.
	///
	bool opEquals()( auto ref const Address addr ) const
	{
		if( addr.length != length || addr.isContainer != isContainer )
			return false;
		
		for( int i; i < length; i++ )
			if( addr[i] != _parts[i] )
				return false;
		
		return true;
	}



	unittest 
	{
		Address addr = Address( "/" );

		assert( addr.isRoot );
		assert( addr.isContainer );
		assert( addr.toString == "/" );
		assert( addr.length == 0 );
		assert( addr.empty == true );

		addr.pushBack( "added" );

		assert( !addr.isRoot );
		assert( addr.isContainer );
		assert( addr.toString == "/added/" );
		assert( addr.length == 1 );
		assert( addr.empty == false );
		assert( addr.front == "added" );
	}

	unittest
	{
		Address addr = Address( "/container/" );

		assert( !addr.isRoot );
		assert( addr.isContainer );
		assert( addr.toString == "/container/" );
		assert( addr.length == 1 );
		assert( addr.front == "container" );

		addr.pushFront( "added" );

		assert( !addr.isRoot );
		assert( addr.isContainer );
		assert( addr.toString == "/added/container/" );
		assert( addr.length == 2 );
		assert( addr.empty == false );
		assert( addr.front == "added" );
		assert( addr.back == "container" );
	}

	unittest
	{
		Address addr = Address( "/container/method" );

		assert( !addr.isRoot );
		assert( addr.isMethod );
		assert( addr.toString == "/container/method" );
		assert( addr.length == 2 );
		assert( addr.front == "container" );
		assert( addr.back == "method" );
		assert( addr.method == "method" );

		addr.pushFront( "added-front" );

		assert( !addr.isRoot );
		assert( addr.isMethod );
		assert( addr.toString == "/added-front/container/method" );
		assert( addr.length == 3 );
		assert( addr.front == "added-front" );
		assert( addr.back == "method" );
		assert( addr.method == "method" );

		addr.pushBack( "added-back" );

		assert( !addr.isRoot );
		assert( addr.isContainer );
		assert( addr.toString == "/added-front/container/method/added-back/" );
		assert( addr.length == 4 );
		assert( addr.front == "added-front" );
		assert( addr.back == "added-back" );
		
		addr.method = "new-method";

		assert( addr.isMethod );
		assert( addr.length == 5 );
		assert( addr.back == "new-method" );
		assert( addr.method == "new-method" );

		addr.method = "other-method";

		assert( addr.isMethod );
		assert( addr.length == 5 );
		assert( addr.back == "other-method" );
		assert( addr.method == "other-method" );
	}

	unittest
	{
		const Address addr = Address( "/container/" ) + "added";

		assert( addr.isContainer );
		assert( addr.length == 2 );
		assert( addr.back == "added" );
	}

	unittest 
	{
		Address pattern = Address( "/container/" );

		assert( pattern.match( Address( "/container/" ) ) );
		assert( !pattern.match( Address( "/container" ) ) );
		assert( !pattern.match( Address( "/container/method" ) ) );
		assert( !pattern.match( Address( "/container/container/" ) ) );
	}

	unittest
	{
		Address pattern = Address( "/method" );

		assert( pattern.match( Address( "/method" ) ) );
		assert( !pattern.match( Address( "/method/" ) ) );
		assert( !pattern.match( Address( "/method/container/" ) ) );
		assert( !pattern.match( Address( "/method/method" ) ) );
	}

	unittest
	{
		Address pattern = Address( "/cont*/" );

		assert( pattern.match( Address( "/container/" ) ) );
		assert( !pattern.match( Address( "/cantainer/" ) ) );
	}

	unittest
	{
		Address pattern = Address( "/cont?ainer/" );

		assert( pattern.match( Address( "/container/") ) );
		assert( !pattern.match( Address( "/conteiner/" ) ) );
	}

	unittest
	{
		Address pattern = Address( "/[a-z]*/cont" );

		assert( pattern.match( Address( "/container/cont" ) ) );
		assert( !pattern.match( Address( "/container/cont2") ) );
		assert( !pattern.match( Address( "/cont-ainer/cont") ) );
	}

	unittest 
	{
		Address pattern = Address( "/[!0-9]*/test!" );

		assert( pattern.match( Address( "/container/test!" ) ) );
		assert( !pattern.match( Address( "/container2/test!" ) ) );
	}

	unittest 
	{
		Address pattern = Address( "/[aA]bc/" );

		assert( pattern.match( Address( "/abc/" ) ) );
		assert( pattern.match( Address( "/Abc/" ) ) );
		assert( !pattern.match( Address( "/xyz/" ) ) );
	}

	unittest 
	{
		Address pattern = Address( "/{cont1,cont2}/" );

		assert( pattern.match( Address( "/cont1/" ) ) );
		assert( pattern.match( Address( "/cont2/" ) ) );
		assert( !pattern.match( Address( "/cont3/" ) ) );
	}

	unittest
	{
		const Address cont = Address( "/container/one/" );
		const Address meth = Address( "/container/method" );

		assert( cont == Address( "/container/one/" ) );
		assert( cont != Address( "/container/one" ) );

		assert( meth == Address( "/container/method" ) );
		assert( meth != Address( "/container/method/" ) );

		assert( cont != meth );
	}

}